Day 25: The Halting Problem
===========================

http://adventofcode.com/2017/day/25

Just make a Turing machine! Performance is for suckers.

* Part 1: 215th place (14:27)
* Part 2: 192nd place (14:45)
