#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "25.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

var rules: [String: [Int: (write: Int, direction: Int, state: String)]] = [:]
var currentState: String = "", checksumValue = 0
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    if components.count == 2 {
        currentState = components[0]
        checksumValue = Int(components[1])!
    } else {
        if rules[components[0]] == nil {
            rules[components[0]] = [:]
        }
        rules[components[0]]![Int(components[1])!] = (
            write: Int(components[2])!,
            direction: components[3] == "Right" ? 1 : -1,
            state: components[4]
        )
    }
}

var cursor = 0
var tape: [Int: Int] = [:]
for _ in 0..<checksumValue {
    let rule = rules[currentState]![tape[cursor] ?? 0]!
    tape[cursor] = rule.write
    currentState = rule.state
    cursor += rule.direction
}
print(tape.values.reduce(0, +))
