//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "19", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

var diagram = input.components(separatedBy: "\n").map { Array($0) }
var height = diagram.count - 1, width = diagram.map { $0.count }.max()!
for row in 0..<diagram.count {
    if diagram[row].count < width {
        diagram[row] += Array(repeating: " ", count: width - diagram[row].count)
    }
}

var row = 0, col = diagram[0].firstIndex(of: "|")!
var rowDelta = 1, colDelta = 0

var pathLetters = "", steps = 1
loop: while true {
    row += rowDelta
    col += colDelta
    if row < 0 || row >= height || col < 0 || col >= width {
        break
    }

    steps += 1

    switch diagram[row][col] {
    case "|", "-":
        break
    case " ":
        steps -= 1
        break loop
    case "+":
        if rowDelta != 0 {
            rowDelta = 0
            if col > 0 && diagram[row][col - 1] != " " {
                colDelta = -1
            } else {
                colDelta = 1
            }
        } else {
            if colDelta != 0 {
                colDelta = 0
                if row > 0 && diagram[row - 1][col] != " " {
                    rowDelta = -1
                } else {
                    rowDelta = 1
                }
            }
        }
    default:
        pathLetters += String(diagram[row][col])
        print(diagram[row][col], "at step", steps)
    }
}

print(pathLetters)
print(steps)

