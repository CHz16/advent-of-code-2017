Day 19: A Series of Tubes
=========================

http://adventofcode.com/2017/day/19

The algorithm assumes the input is "well-formed" and "unambiguous" enough to work it like this:

* If we're on a space or fall off the diagram altogether, quit.
* If we're on `|` or `-`, just keep going. It doesn't bother comparing that with the direction we're going, because of paths that cross over.
* If we're on a `+`, check if the square 90 degrees in one direction isn't a space, and if so, start moving in that direction. Otherwise, start moving in the other 90 degree direction.
* If we're on anything else (i.e. a letter), add that to the output.

The padding of the diagram's rows in lines 13-17 is to round out the length of each row to the width of the diagram, because the input ends at the last non-space character. Just removed the need for some existence checking when turning or leaving the path.

The subtraction on line 31 is to delete the extra step taken past the end of the path from the count.

* Part 1: 274th place (25:00)
* Part 2: 286th place (29:23)
