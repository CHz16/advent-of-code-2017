Day 10: Knot Hash
=================

http://adventofcode.com/2017/day/10

I don't really have any comments on these solutions this year haha, again I kind of just did the thing. The only tricky part is that I added the length of the list you use to the puzzle input for testing purposes, since the part 1 example data uses a smaller list, and my part 2 solution will hash every line in the given file, which means it will hash that size too before getting to the real input. Maybe don't submit the hashed size like I did.

Also maybe ignore the chained `map.reduce` monstrosity on the last couple of lines of part 2 that change the dense hash into the proper string output.
