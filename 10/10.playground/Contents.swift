//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "10", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let lines = input.components(separatedBy: "\n")
let listLength = Int(lines[0])!
let lengths = lines[1].components(separatedBy: ",").compactMap { Int($0) }

var list = Array(0..<listLength)
var skip = 0, position = 0
for length in lengths {
    let range = position..<(position + length)
    let tempList = list + list
    let reversedSublist = tempList[range].reversed()
    for (i, element) in reversedSublist.enumerated() {
        list[(position + i) % listLength] = element
    }

    position = (position + length + skip) % listLength
    skip += 1
}
print(list[0] * list[1])


// Part 2

print("--------------")

let hashUrl = Bundle.main.url(forResource: "10", withExtension: "txt")
let hashInput = try String(contentsOf: hashUrl!, encoding: String.Encoding.utf8)

let hashListLength = 256

hashInput.enumerateLines { (line, stop) in
    let lengths = line.unicodeScalars.map { Int($0.value) } + [17, 31, 73, 47, 23]

    var list = Array(0..<hashListLength)
    var skip = 0, position = 0
    for _ in 0..<64 {
        for length in lengths {
            let range = position..<(position + length)
            let tempList = list + list
            let reversedSublist = tempList[range].reversed()
            for (i, element) in reversedSublist.enumerated() {
                list[(position + i) % hashListLength] = element
            }

            position = (position + length + skip) % hashListLength
            skip += 1
        }
    }

    var denseHash: [Int] = []
    for chunk in 0..<16 {
        let range = (chunk * 16)..<(chunk * 16 + 16)
        denseHash.append(list[range].reduce(0, ^))
    }
    print(denseHash.map {
        let hexes = [0: "0", 1: "1", 2: "2", 3: "3", 4: "4", 5: "5", 6: "6", 7: "7", 8: "8", 9: "9", 10: "a", 11: "b", 12: "c", 13: "d", 14: "e", 15: "f"]
        return hexes[$0 / 16]! + hexes[$0 % 16]!
    }.reduce("", +))
}
