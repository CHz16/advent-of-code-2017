Day 17: Spinlock
================

http://adventofcode.com/2017/day/17

Part 1, I'm not sure if there's a better solution besides making a list and inserting 2017 elements into it.

Part 2, though, fifty million elements is a lot! But notice that 0 will always be the first element in the list, because we always insert the next element **after** the current position. This means that we don't need to store the buffer at all; all we need to do is keep track of where the current position **would** be for each element as we "insert" it, and if we'd be inserting something after position zero, then save that as current element that's after 0.

Moved up 127 places for part 2, pretty good gains!

* Part 1: 195th place (8:49)
* Part 2: 68th place (14:20)
