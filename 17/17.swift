#!/usr/bin/env xcrun swift

import Cocoa

//let input = 3
let input = 359


// Part 1

var buffer = [0]
var position = 0
for i in 1...2017 {
    position = (position + input) % buffer.count + 1
    buffer.insert(i, at: position)
}
print(buffer[(position - 1)...(position + 1)])


// Part 2

print("--------------")

position = 0
for i in 1...50000000 {
    position = (position + input) % i + 1
    if position == 1 {
        print(i)
    }
}
