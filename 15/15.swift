#!/usr/bin/env xcrun swift

import Cocoa

//let aInput = 65, bInput = 8921
let aInput = 591, bInput = 393
let aFactor = 16807, bFactor = 48271


// Part 1

var aValue = aInput, bValue = bInput
var matches = 0
for _ in 0..<40000000 {
    aValue = (aValue * aFactor) % 2147483647
    bValue = (bValue * bFactor) % 2147483647

    if aValue % 65536 == bValue % 65536 {
        print(aValue, bValue)
        matches += 1
    }
}
print(matches)


// Part 2

print("--------------")

aValue = aInput
bValue = bInput
matches = 0
for _ in 0..<5000000 {
    repeat {
        aValue = (aValue * aFactor) % 2147483647
    } while aValue % 4 != 0
    repeat {
        bValue = (bValue * bFactor) % 2147483647
    } while bValue % 8 != 0

    if aValue % 65536 == bValue % 65536 {
        print(aValue, bValue)
        matches += 1
    }
}
print(matches)
