Day 15: Dueling Generators
=================================

http://adventofcode.com/2017/day/15

Bitwise arithmetic! Pretty basic, just do some math to make some numbers, then mask out the last 16 bits of each and check if those match. I just ended up doing it with a modulo of 2^16 and then checking equality haha.

* Part 1: 120th place (6:19)
* Part 2: 78th place (8:31)
