//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "1", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

let values: [Character: Int] = ["1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9]

input.enumerateLines { (line, stop) in
    let captcha = line + String(line.first!)

    var checksum = 0
    var index = captcha.index(after: captcha.startIndex)
    while index != captcha.endIndex {
        if captcha[index] == captcha[captcha.index(before: index)] {
            if let value = values[captcha[index]] {
                checksum += value
            }
        }
        index = captcha.index(after: index)
    }

    print(line, checksum)
}


// Part 2

print("--------------")

input.enumerateLines { (line, stop) in
    var checksum = 0
    var first = line.startIndex
    var second = line.index(first, offsetBy: line.count / 2)
    while second != line.endIndex {
        if line[first] == line[second] {
            if let value = values[line[first]] {
                checksum += value * 2
            }
        }
        first = line.index(after: first)
        second = line.index(after: second)
    }

    print(line, checksum)
}
