Day 1: Inverse Captcha
======================

http://adventofcode.com/2017/day/1

Extremely simple string scanning problem. The only "optimizations" I made were adding the first character to the end of the string in part 1 to avoid having to special case the comparison between the first and last characters, and scanning through only half the string in part 2 and adding every value twice (because the inputs all have an even number of characters, so if character A is paired with B, then B will end up being paired with A).

Both parts of this problem could've been done with a more general solution with a customizable distance between the indices, but that would've required wrapping logic, which both of my shortcuts avoided.
