#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "18.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    typealias Register = String
    enum Value {
        case literal(Int), register(Register)

        init(_ x: String) {
            if let x = Int(x) {
                self = .literal(x)
            } else {
                self = .register(x)
            }
        }
    }

    enum Instruction {
        case send(Value), set(Register, Value), add(Register, Value), multiply(Register, Value), modulo(Register, Value), receive(Register), jumpGreaterThanZero(Value, Value)
    }


    let id: Int, sendDestination: Int
    var registers: [String: Int]
    var code: [Instruction]
    var pc = 0
    var valuesSent = 0, valuesReceived = 0

    init(assembly: String, id: Int, sendDestination: Int) {
        self.id = id
        self.sendDestination = sendDestination
        self.registers = ["p": id]

        code = []
        for line in assembly.components(separatedBy: "\n") {
            if line == " " {
                continue
            }

            let tokens = line.components(separatedBy: " ")
            switch tokens[0] {
            case "snd":
                code.append(.send(Value(tokens[1])))
            case "set":
                code.append(.set(tokens[1], Value(tokens[2])))
            case "add":
                code.append(.add(tokens[1], Value(tokens[2])))
            case "mul":
                code.append(.multiply(tokens[1], Value(tokens[2])))
            case "mod":
                code.append(.modulo(tokens[1], Value(tokens[2])))
            case "rcv":
                code.append(.receive(tokens[1]))
            case "jgz":
                code.append(.jumpGreaterThanZero(Value(tokens[1]), Value(tokens[2])))
            default:
                break
            }
        }
    }

    mutating func evaluate(value: Value) -> Int {
        switch value {
        case let .literal(n):
            return n
        case let .register(r):
            return getRegisterValue(register: r)
        }
    }

    mutating func getRegisterValue(register: String) -> Int {
        if registers[register] == nil {
            registers[register] = 0
        }
        return registers[register]!
    }


    enum ExecutionStatus { case running, waiting, terminated }
    mutating func step() -> ExecutionStatus {
        if pc < 0 || pc >= code.count {
            return .terminated
        }
        let instruction = code[pc]
        pc += 1

        switch instruction {
        case let .send(value):
            valuesSent += 1
            queues[sendDestination].append(evaluate(value: value))
            print(id, "sending", evaluate(value: value))
        case let .set(destination, value):
            registers[destination] = evaluate(value: value)
        case let .add(destination, value):
            registers[destination] = getRegisterValue(register: destination) + evaluate(value: value)
        case let .multiply(destination, value):
            registers[destination] = getRegisterValue(register: destination) * evaluate(value: value)
        case let .modulo(destination, value):
            registers[destination] = getRegisterValue(register: destination) % evaluate(value: value)
        case let .receive(destination):
            if queues[id].isEmpty {
                pc -= 1
                return .waiting
            }
            valuesReceived += 1
            registers[destination] = queues[id].removeFirst()
            print(id, "receiving", registers[destination]!)
        case let .jumpGreaterThanZero(value, offset):
            if evaluate(value: value) > 0 {
                pc += evaluate(value: offset) - 1
            }
        }

        return .running
    }

}


// Part 1

var queues: [[Int]] = [[]]
var vm = VM(assembly: input, id: 0, sendDestination: 0)
while vm.step() == .running && vm.valuesReceived == 0 {

}


// Part 2

print("--------------")

queues = [[], []]
var vm0 = VM(assembly: input, id: 0, sendDestination: 1)
var vm1 = VM(assembly: input, id: 1, sendDestination: 0)
while true {
    let vm0Status = vm0.step()
    let vm1Status = vm1.step()
    if vm0Status != .running && vm1Status != .running {
        break
    }
}
print("VM 0 values sent:", vm0.valuesSent)
print("VM 1 values sent:", vm1.valuesSent)

