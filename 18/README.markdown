Day 18: Duet
============

http://adventofcode.com/2017/day/18

Another year, another VM...

Part 2 adds concurrency, which is neat!

* Part 1: 173rd place (16:42)
* Part 2: 100th place (41:02)
