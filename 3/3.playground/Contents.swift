//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "3", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

input.enumerateLines { (line, stop) in
    let id = Int(line)!
    if id == 1 {
        print(1, 0)
        return
    }

    let spiral = Int(Float(id).squareRoot().rounded(.up) / 2) + 1
    let strideLength = (spiral - 1) * 2
    let lastSquare = (spiral - 2) * 2 + 1
    let index = (id - lastSquare * lastSquare - 1) % strideLength

    let lowestPoint = strideLength / 2 - 1
    let pos = abs(index - lowestPoint)
    print(id, lastSquare + pos - lowestPoint);
}


// Part 2

print("--------------")

var cache: [Int: [Int: Int]] = [:]
cache[0] = [0: 1]
func checkSquare(_ x: Int, _ y: Int) -> Int {
    if let col = cache[x] {
        if let value = col[y] {
            return value
        }
    }
    return 0
}
func setSquare(_ x: Int, _ y: Int, value: Int) {
    if cache[x] == nil {
        cache[x] = [:]
    }
    cache[x]![y] = value
}

input.enumerateLines { (line, stop) in
    let limit = Int(line)!
    var x = 0, y = 0
    var xDir = 1, yDir = 0
    var i = 1
    while true {
        let value = cache[x]?[y] ?? checkSquare(x - 1, y - 1) + checkSquare(x - 1, y) + checkSquare(x - 1, y + 1) + checkSquare(x, y + 1) + checkSquare(x + 1, y + 1) + checkSquare(x + 1, y) + checkSquare(x + 1, y - 1) + checkSquare(x, y - 1)
        setSquare(x, y, value: value)

        if (value > limit) {
            print(i, value)
            break
        }

        i += 1
        x += xDir
        y += yDir
        if y >= 0 && x == y + 1 {
            (xDir, yDir) = (0, -1)
        } else if y < 0 && x == -y {
            (xDir, yDir) = (-1, 0)
        } else if y < 0 && x == y {
            (xDir, yDir) = (0, 1)
        } else if y > 0 && x == -y {
            (xDir, yDir) = (1, 0)
        }
    }
}
