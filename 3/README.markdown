Day 3: Spiral Memory
====================

http://adventofcode.com/2017/day/3

I wasted a half an hour coming up with a closed form calculation for part 1, expecting part 2 to just be the same problem with a gigantic input, and then it wasn't at all whoops

So anyway, the closed form solution works by noticing that the answers follow this sequence:

* Square 1: `0`
* Squares 2-9: `1 2 | 1 2 | 1 2 | 1 2`
* Squares 10-25: `3 2 3 4 | 3 2 3 4 | 3 2 3 4 | 3 2 3 4`
* Squares 26-49: `5 4 3 4 5 6 | 5 4 3 4 5 6 | 5 4 3 4 5 6 | 5 4 3 4 5 6`
* Squares 50-81: `7 6 5 4 5 6 7 8 | 7 6 5 4 5 6 7 8 | 7 6 5 4 5 6 7 8 | 7 6 5 4 5 6 7 8`

The solution for part 2 just trucks along every single square in order.

* Part 1: 628th place (34:37)
* Part 2: 527th place (1:04:27)
