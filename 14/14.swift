#!/usr/bin/env xcrun swift

import Cocoa

//let input = "flqrgnkx"
let input = "ugkiagan"


// Part 1

let hashListLength = 256
func knotHash(_ s: String) -> [Int] {
    let lengths = s.unicodeScalars.map { Int($0.value) } + [17, 31, 73, 47, 23]

    var list = Array(0..<hashListLength)
    var skip = 0, position = 0
    for _ in 0..<64 {
        for length in lengths {
            let range = position..<(position + length)
            let tempList = list + list
            let reversedSublist = tempList[range].reversed()
            for (i, element) in reversedSublist.enumerated() {
                list[(position + i) % hashListLength] = element
            }

            position = (position + length + skip) % hashListLength
            skip += 1
        }
    }

    var denseHash: [Int] = []
    for chunk in 0..<16 {
        let range = (chunk * 16)..<(chunk * 16 + 16)
        denseHash.append(list[range].reduce(0, ^))
    }
    return denseHash
}

let printable = [0: "....", 1: "...#", 2: "..#.", 3: "..##", 4: ".#..", 5: ".#.#", 6: ".##.", 7: ".###", 8: "#...", 9: "#..#", 10: "#.#.", 11: "#.##", 12: "##..", 13: "##.#", 14: "###.", 15: "####"]
let popCounts = [0: 0, 1: 1, 2: 1, 3: 2, 4: 1, 5: 2, 6: 2, 7: 3, 8: 1, 9: 2, 10: 2, 11: 3, 12: 2, 13: 3, 14: 3, 15: 4]

var popCount = 0
var grid: [String] = []
for i in 0..<128 {
    let hash = knotHash(input + "-" + String(i))
    var row = ""
    for byte in hash {
        row += printable[byte / 16]! + printable[byte % 16]!
        popCount += popCounts[byte / 16]! + popCounts[byte % 16]!
    }
    grid.append(row)
    print(row)
}
print(popCount)


// Part 2

print("--------------")

func charAt(row: Int, col: Int) -> Character {
    let row = grid[row]
    return row[row.index(row.startIndex, offsetBy: col)]
}

var visited: [[Bool]] = Array(repeating: Array(repeating: false, count: 128), count: 128)
var regions = 0
for row in 0..<128 {
    for col in 0..<128 {
        if visited[row][col] || charAt(row: row, col: col) == "." {
            continue
        }

        var stack = [(row, col)]
        while !stack.isEmpty {
            let (markingRow, markingCol) = stack.popLast()!
            if markingRow < 0 || markingRow > 127 || markingCol < 0 || markingCol > 127 || visited[markingRow][markingCol] || charAt(row: markingRow, col: markingCol) == "." {
                continue
            }

            visited[markingRow][markingCol] = true
            stack.append((markingRow + 1, markingCol))
            stack.append((markingRow - 1, markingCol))
            stack.append((markingRow, markingCol + 1))
            stack.append((markingRow, markingCol - 1))
        }

        regions += 1
    }
}
print(regions)

