Day 14: Disk Defragmentation
============================

http://adventofcode.com/2017/day/14

Part 1 is basically just rehashing day 10 (knot hashes), and part 2 is basically rehashing day 12 (counting connected groups in a graph).

I started late so I didn't time myself, but I jumped more than 300 places between part 1 and part 2, so I guess people had problems with the second one!

* Part 1: 974th place
* Part 2: 640th place
