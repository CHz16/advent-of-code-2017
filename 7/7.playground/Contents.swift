//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "7", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

struct Program {
    let id: String
    var weight = 0
    var children: [String] = []
    var parent: String?
    var cleared = false

    init(_ inId: String) {
        id = inId
    }

    func supportedWeight() -> Int {
        return weight + children.map { tower[$0]!.supportedWeight() }.reduce(0, +)
    }
}

var tower: [String: Program] = [:]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")

    if tower[components[0]] == nil {
        tower[components[0]] = Program(components[0])
    }
    tower[components[0]]!.weight = Int(components[1])!
    for child in components.dropFirst(2) {
        tower[components[0]]!.children.append(child)

        if tower[child] == nil {
            tower[child] = Program(child)
        }
        tower[child]!.parent = components[0]
    }
}

var bottomProgram: Program?
for (_, program) in tower {
    if program.parent == nil {
        bottomProgram = program
        print(bottomProgram!)
    }
}


// Part 2

print("--------------")

var currentProgram = bottomProgram!
while true {
    if currentProgram.children.count == 1 {
        currentProgram = tower[currentProgram.children[0]]!
        continue
    }

    let subtreeWeights = currentProgram.children.map { tower[$0]!.supportedWeight() }
    if Set(subtreeWeights).count == 1 {
        currentProgram = tower[currentProgram.parent!]!
        break
    }

    let firstWeight = subtreeWeights[0]
    let secondWeight = subtreeWeights[1]
    let thirdWeight = subtreeWeights[2]
    if firstWeight != secondWeight {
        if firstWeight == thirdWeight {
            currentProgram = tower[currentProgram.children[1]]!
        } else {
            currentProgram = tower[currentProgram.children[0]]!
        }
        continue
    } else {
        for (i, weight) in subtreeWeights.dropFirst(2).enumerated() {
            if weight != firstWeight {
                currentProgram = tower[currentProgram.children[i + 2]]!
                break
            }
        }
    }
}

print(currentProgram)
print(currentProgram.children.map { tower[$0]!.supportedWeight() })

