Day 7: Recursive Circus
=======================

http://adventofcode.com/2017/day/7

The first part just constructs the tree and looks for the node with no parent, very straightforward.

The second part searches until it finds a node that has three or more subtrees of unequal weight, where the subtrees of the child that doesn't match the others are all equal. This approach does NOT work in the general case, failing with nodes of two children and leaf nodes, but it worked with my input lmao

* Part 1: 522nd place (14:25)
* Part 2: 519th place (56:42)
