#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "24.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

var bridgeComponents: [(Int, Int)] = []
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: "/")
    bridgeComponents.append((Int(components[0])!, Int(components[1])!))
}

var strongestBridgeStrength = 0, longestBridgeLength = 0, longestBridgeStrength = 0
func findBridges(bridge: [(Int, Int)], openPort: Int, unusedComponents: [(Int, Int)]) {
    let currentBridgeStrength = bridge.reduce(0) { $0 + $1.0 + $1.1 }
    if currentBridgeStrength > strongestBridgeStrength {
        strongestBridgeStrength = currentBridgeStrength
        print("new strongest:", strongestBridgeStrength, bridge)
    }

    if bridge.count > longestBridgeLength {
        longestBridgeLength = bridge.count
        longestBridgeStrength = currentBridgeStrength
        print("new longest:", longestBridgeLength, longestBridgeStrength, bridge)
    } else if bridge.count == longestBridgeLength && currentBridgeStrength > longestBridgeStrength {
        longestBridgeStrength = currentBridgeStrength
        print("new longest:", longestBridgeLength, longestBridgeStrength, bridge)
    }

    for i in 0..<unusedComponents.count {
        if openPort == unusedComponents[i].0 {
            var newBridge = bridge, newUnusedComponents = unusedComponents
            newBridge.append(unusedComponents[i])
            newUnusedComponents.remove(at: i)
            findBridges(bridge: newBridge, openPort: newBridge.last!.1, unusedComponents: newUnusedComponents)
        } else if openPort == unusedComponents[i].1 {
            var newBridge = bridge, newUnusedComponents = unusedComponents
            newBridge.append(unusedComponents[i])
            newUnusedComponents.remove(at: i)
            findBridges(bridge: newBridge, openPort: newBridge.last!.0, unusedComponents: newUnusedComponents)
        }
    }
}
findBridges(bridge: [], openPort: 0, unusedComponents: bridgeComponents)


print("--------------")
print(strongestBridgeStrength)
print("--------------")
print(longestBridgeStrength)
