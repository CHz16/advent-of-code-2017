//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "11", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

let deltas = ["n": [0, 1, -1], "s": [0, -1, 1], "ne": [1, 0, -1], "sw": [-1, 0, 1], "nw": [-1, 1, 0], "se": [1, -1, 0]]

func hexDistance(_ coords: [Int]) -> Int {
    return coords.map(abs).max()!
}

input.enumerateLines { (line, stop) in
    var furthestDistance = 0
    var coords = [0, 0, 0]
    for dir in line.components(separatedBy: ",") {
        coords = zip(coords, deltas[dir]!).map(+)
        furthestDistance = max(furthestDistance, hexDistance(coords))
    }
    print(hexDistance(coords), furthestDistance, line)
}
