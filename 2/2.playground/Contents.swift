//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "2", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var checksum = 0
input.enumerateLines { (line, stop) in
    let numbers = line.components(separatedBy: " ").compactMap { Int($0) }
    print(numbers.max()!, numbers.min()!)
    checksum += numbers.max()! - numbers.min()!
}
print(checksum)


// Part 2

print("--------------")

checksum = 0
input.enumerateLines { (line, stop) in
    let numbers = line.components(separatedBy: " ").compactMap { Int($0) }
    outer: for firstIndex in 0..<(numbers.count - 1) {
        for secondIndex in (firstIndex + 1)..<numbers.count {
            var first = numbers[firstIndex]
            var second = numbers[secondIndex]
            if first < second {
                (first, second) = (second, first)
            }
            if first % second == 0 {
                print(first, second)
                checksum += first / second
                continue outer
            }
        }
    }
}
print(checksum)
