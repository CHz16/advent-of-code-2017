Day 2: Corruption Checksum
==========================

http://adventofcode.com/2017/day/2

More going through strings and doing some calculations with the numbers in them.

The only "tricky" business here is the use of `completeMap` on lines 13 & 26 to filter out optionals. The input file is formatted into columns using multiple spaces, which means that the array returned by `components(separatedBy: " ")` will include empty strings in between the numeric strings, and the `{ Int($0) }` closure used with `completeMap` will return optionals that are nil for those empty strings. If we used `map`, then we'd get back `[Int?]` because of that, but `completeMap` will send us an `[Int]` by unwrapping the optionals and tossing away the `nil`s, which is excellent.
