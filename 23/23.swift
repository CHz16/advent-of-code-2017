#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "23.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    typealias Register = String
    enum Value {
        case literal(Int), register(Register)

        init(_ x: String) {
            if let x = Int(x) {
                self = .literal(x)
            } else {
                self = .register(x)
            }
        }
    }

    enum Instruction {
        case set(Register, Value), subtract(Register, Value), multiply(Register, Value), jumpNotZero(Value, Value)
    }


    var registers: [String: Int] = [:]
    var code: [Instruction]
    var pc = 0
    var multipliesMade = 0

    init(assembly: String) {
        code = []
        for line in assembly.components(separatedBy: "\n") {
            if line == " " {
                continue
            }

            let tokens = line.components(separatedBy: " ")
            switch tokens[0] {
            case "set":
                code.append(.set(tokens[1], Value(tokens[2])))
            case "sub":
                code.append(.subtract(tokens[1], Value(tokens[2])))
            case "mul":
                code.append(.multiply(tokens[1], Value(tokens[2])))
            case "jnz":
                code.append(.jumpNotZero(Value(tokens[1]), Value(tokens[2])))
            default:
                break
            }
        }
    }

    mutating func evaluate(value: Value) -> Int {
        switch value {
        case let .literal(n):
            return n
        case let .register(r):
            return getRegisterValue(register: r)
        }
    }

    mutating func getRegisterValue(register: String) -> Int {
        if registers[register] == nil {
            registers[register] = 0
        }
        return registers[register]!
    }


    enum ExecutionStatus { case running, terminated }
    mutating func step() -> ExecutionStatus {
        if pc < 0 || pc >= code.count {
            return .terminated
        }
        let instruction = code[pc]
        pc += 1

        switch instruction {
        case let .set(destination, value):
            registers[destination] = evaluate(value: value)
        case let .subtract(destination, value):
            registers[destination] = getRegisterValue(register: destination) - evaluate(value: value)
        case let .multiply(destination, value):
            multipliesMade += 1
            registers[destination] = getRegisterValue(register: destination) * evaluate(value: value)
        case let .jumpNotZero(value, offset):
            if evaluate(value: value) != 0 {
                pc += evaluate(value: offset) - 1
            }
        }

        return .running
    }

}


// Part 1

var vm = VM(assembly: input)
while vm.step() == .running { }
print(vm.multipliesMade)


// Part 2

print("--------------")

let primesUrl = URL(fileURLWithPath: "primes.txt")
let primesInput = try String(contentsOf: primesUrl, encoding: String.Encoding.utf8)

var numbers = Array(stride(from: 106700, to: 123701, by: 17))
var composites = numbers.count
primesInput.enumerateLines { (line, stop) in
    if let _ = numbers.firstIndex(of: Int(line)!) {
        composites -= 1
    }
}
print(composites)
