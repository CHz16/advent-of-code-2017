Day 23: Coprocessor Conflagration
=================================

http://adventofcode.com/2017/day/23

First you spend two minutes adding a couple of instructions to your VM from day 18, and then you spend an hour figuring out what the hell the code is doing so you can make it run in less than a trillion years. I hate debugging assembly lmao

I'm not going to explain exactly what the program is doing because that's the ~puzzle~, but my code that calculates the answer should make it extremely clear.

* Part 1: 121st place (5:27)
* Part 2: 135th place (1:01:27)
