#!/usr/bin/env xcrun swift -F .. -framework CryptoSwift

import CryptoSwift

let input = "uqwqemis"


// Part 1

print(input.md5())


// Part 2

print("--------------")
