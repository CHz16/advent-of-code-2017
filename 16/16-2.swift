#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "16.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


var indexPermutation = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
var inverseCharPermutation = ["a": "a", "b": "b", "c": "c", "d": "d", "e": "e", "f": "f", "g": "g", "h": "h", "i": "i", "j": "j", "k": "k", "l": "l", "m": "m", "n": "n", "o": "o", "p": "p"]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    if components[0] == "s" {
        let slice = indexPermutation.count - Int(components[1])!
        indexPermutation = Array(indexPermutation[slice...] + indexPermutation[..<slice])
    } else if components[0] == "x" {
        let positionA = Int(components[1])!
        let positionB = Int(components[2])!
        (indexPermutation[positionB], indexPermutation[positionA]) = (indexPermutation[positionA], indexPermutation[positionB])
    } else if components[0] == "p" {
        let charA = components[1]
        let charB = components[2]
        (inverseCharPermutation[charB], inverseCharPermutation[charA]) = (inverseCharPermutation[charA]!, inverseCharPermutation[charB]!)
    }
}
var charPermutation: [String: String] = [:]
for (key, value) in inverseCharPermutation {
    charPermutation[value] = key
}


var programs = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"]

for n in 0..<10 {
    print(n * 100000, programs.joined())
    for _ in 0..<100000 {
        programs = indexPermutation.map { programs[$0] }
        programs = programs.map { charPermutation[$0]! }
    }
}
print(100000, programs.joined())
