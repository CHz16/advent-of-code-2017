Day 16: Permutation Promenade
=============================

http://adventofcode.com/2017/day/16

Part 1 has you compute a permutation of a list. Part 2 has you permute a list a billion times. The trick to part 2 is finding when a cycle takes place after repeated permutations and using that to immediately calculate the billionth permutation.

I flubbed the part 2 by trying to be clever: taking the part 1 answer and repeatedly applying that permutation over and over. That doesn't work! The partner command will swap different indices in successive dances, so you do actually need to execute the whole dance again every time.

* Part 1: 209th place (12:09)
* Part 2: 256th place (42:23)

Addendum
--------

I also had an idea for simplifying the instruction list into two permutations: an index-based one that applies all the Spin and Exchange instructions at once, and a character-based one that applies all the Partner instructions at once. I wanted to see how "well" it would work for brute forcing, so I ran it over a million iterations, and it took 35 seconds. So, multiply that by a thousand and you get about 9 hours and 45 minutes. Not great! But it would make my part 2 solution way faster at least.

So that's in `16-2.swift`.
