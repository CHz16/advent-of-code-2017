#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "16.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func permute(_ programs: [String]) -> [String] {
    var programs = programs
    input.enumerateLines { (line, stop) in
        let components = line.components(separatedBy: " ")
        if components[0] == "s" {
            let slice = programs.count - Int(components[1])!
            programs = Array(programs[slice...] + programs[..<slice])
        } else if components[0] == "x" {
            let positionA = Int(components[1])!
            let positionB = Int(components[2])!
            (programs[positionB], programs[positionA]) = (programs[positionA], programs[positionB])
        } else if components[0] == "p" {
            let positionA = programs.firstIndex(of: components[1])!
            let positionB = programs.firstIndex(of: components[2])!
            (programs[positionB], programs[positionA]) = (programs[positionA], programs[positionB])
        }
    }
    return programs
}

print(permute(["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"]).joined())


// Part 2

print("--------------")

var programs = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p"]
var seenOrders = [programs.joined()]

for _ in 0..<1000000000 {
    programs = permute(programs)

    let order = programs.joined()
    if let cycleStart = seenOrders.firstIndex(of: order) {
        let cycleLength = seenOrders.count - cycleStart
        print("cycle start:", cycleStart)
        print("cycle length:", cycleLength)
        print(seenOrders[cycleStart + (1000000000 - cycleStart) % cycleLength])
        break
    }
    seenOrders.append(order)
}
