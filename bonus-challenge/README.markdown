Bonus Challenge
====================

https://www.reddit.com/r/adventofcode/comments/72aizu/bonus_challenge/

Two part problem that reuses code from 2016.

The first part uses the VM from day 25, except that the `out` command is now fed ASCII character codes.

Running this gives a list of commands for the day 8 LCD. This can be run directly from the day 8 code.
