Advent of Code 2017
===================

These are my solutions in Swift to [Advent of Code 2017](http://adventofcode.com/2017), a kind of fun series of 25 programming puzzles that ran in December 2017. I figured I'd throw these up on the web somewhere, because why not. If I was competing for a leaderboard spot that day (that is, I started coding at 9 PM Pacific), then the readme file for that day will give my time and place.

While the challenges had no time limit, there was a leaderboard that showed (essentially) the fastest 100 solvers of each day's problem, so naturally I tried to solve them as quickly as possible. So, most of the solutions here are simply what left my fingers the fastest. That's speedcoding for you.

I started writing every single challenge in a playground, because they're fantastic tools, but a lot of them I ended up running from the command line after finishing the algorithm because it was way faster. Usually this was because something was looping thousands of times and timelines collecting all those intermediate values massively slowed things down. So if you see a playground solution, that means it ran well enough that I didn't have to drop down and get a boost with `xcrun`.

Every user had their puzzle inputs randomly selected from a pool of possibilities, so the inputs you see in these files may not be the same ones you'd get were you to sign up. I also tend to preprocess them in a text editor to make them way easier to parse.

Past years have required MD5 hashing, so I built [Marcin Krzyżanowski's CryptoSwift framework](https://github.com/krzyzanowskim/CryptoSwift) for linking, but it turns out that was totally unnecessary. Cool beans!

The `skeleton-` directories are just skeleton projects to copy for a given day. This saved me two seconds over the previous years, where I had to search previous days for a project in the correct format.

* Best placement for any part: day 13, part 1 (56th place)
* Days with extra optimizations: 16, 21

*See also: [2015](https://bitbucket.org/CHz16/advent-of-code-2015), [2016](https://bitbucket.org/CHz16/advent-of-code-2016), [2018](https://bitbucket.org/CHz16/advent-of-code-2018), [2019](https://bitbucket.org/CHz16/advent-of-code-2019), [2020](https://bitbucket.org/CHz16/advent-of-code-2020), [2021](https://bitbucket.org/CHz16/advent-of-code-2021), [2022](https://bitbucket.org/CHz16/advent-of-code-2022)*
