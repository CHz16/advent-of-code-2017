#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "20.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

struct Particle {
    var position: (x: Int, y: Int, z: Int)
    var velocity: (x: Int, y: Int, z: Int)
    var acceleration: (x: Int, y: Int, z: Int)

    init(_ spec: String) {
        let components = spec.components(separatedBy: ",").map { Int($0)! }
        position = (components[0], components[1], components[2])
        velocity = (components[3], components[4], components[5])
        acceleration = (components[6], components[7], components[8])
    }

    mutating func step() {
        velocity.x += acceleration.x
        velocity.y += acceleration.y
        velocity.z += acceleration.z
        position.x += velocity.x
        position.y += velocity.y
        position.z += velocity.z
    }

    var distance: Int {
        return abs(position.x) + abs(position.y) + abs(position.z)
    }
    var totalVelocity: Int {
        return abs(velocity.x) + abs(velocity.y) + abs(velocity.z)
    }
    var totalAcceleration: Int {
        return abs(acceleration.x) + abs(acceleration.y) + abs(acceleration.z)
    }

    func position(atTime t: Int) -> (x: Int, y: Int, z: Int) {
        return (
            x: position.x + t * velocity.x + t * (t + 1) * acceleration.x / 2,
            y: position.y + t * velocity.y + t * (t + 1) * acceleration.y / 2,
            z: position.z + t * velocity.z + t * (t + 1) * acceleration.z / 2
        )
    }
}

var particles: [Particle] = []
var slowestParticle = 0, slowestParticleAcceleration = Int.max, slowestParticleVelocity = Int.max
input.enumerateLines { (line, stop) in
    let particle = Particle(line)
    particles.append(particle)

    let totalAcceleration = particle.totalAcceleration, totalVelocity = particle.totalVelocity
    if totalAcceleration < slowestParticleAcceleration || (totalAcceleration == slowestParticleAcceleration && totalVelocity < slowestParticleVelocity) {
        slowestParticle = particles.count - 1
        slowestParticleAcceleration = totalAcceleration
        slowestParticleVelocity = totalVelocity
        print(slowestParticle, slowestParticleAcceleration, slowestParticleVelocity)
    }
}
print("^^^ slowest particle ^^^")


// Part 2

print("--------------")

var collisions: [(t: Int, i: Int, j: Int)] = []
for i in 0..<(particles.count - 1) {
    for j in (i + 1)..<particles.count {
        // Calculate the time(s) t at which particles i & j will have the same x coordinate
        // The x coordinate at time t is i.xPos + t * i.xVel + (t * (t + 1) / 2) * i.xAcc
        // We find by equating that formula for the two particles and solving the quadratic equation for t
        // To avoid the division by 2 possibly making some of the numbers here nonintegral, we'll multiply all the coefficients (a, b, and c) by 2. This won't change the roots of the equation.
        let a = particles[j].acceleration.x - particles[i].acceleration.x
        var solutions: [Int] = []
        if a == 0 {
            // If a == 0, then the particles have the same acceleration, so the collision formula simplifies to t = (i.xPos - j.xPos) / (j.xVel - i.xVel)
            // If they have the same velocity, then they'll never collide
            // We also work entirely with integral steps, so if t isn't a whole number then they won't collide
            if particles[j].velocity.x == particles[i].velocity.x {
                continue
            }
            if (particles[i].position.x - particles[j].position.x) % (particles[j].velocity.x - particles[i].velocity.x) != 0 {
                continue
            }
            let t = (particles[i].position.x - particles[j].position.x) / (particles[j].velocity.x - particles[i].velocity.x)
            if t < 0 {
                continue
            }
            solutions.append(t)
        } else {
            // If a != 0, then one or both particles are accelerating, so we do have to solve the quadratic formula.
            // There are a bunch of checks here to make sure the collision times are integral, because time steps are integral.
            // It's quite possible that there are two collision times for a pair of particles!
            let b = 2 * (particles[j].velocity.x - particles[i].velocity.x) + a
            let c = 2 * (particles[j].position.x - particles[i].position.x)
            let discriminant = b * b - 4 * a * c
            if discriminant < 0 {
                continue
            }

            let sqrtDiscriminant = Int(sqrt(Double(discriminant)))
            if sqrtDiscriminant * sqrtDiscriminant != discriminant {
                continue
            }

            let numerator1 = -b - sqrtDiscriminant
            let numerator2 = -b + sqrtDiscriminant
            let denominator = 2 * a
            if numerator1 % denominator == 0 && numerator1 / denominator >= 0 {
                solutions.append(numerator1 / denominator)
            }
            // I'm not sure there can ever be one repeated root, but just in case
            if numerator2 % denominator == 0 && numerator2 / denominator >= 0 && numerator1 / denominator != numerator2 / denominator {
                solutions.append(numerator2 / denominator)
            }

            if solutions.isEmpty {
                continue
            }
        }

        // Now that we know the possible collision times, check if an actual collision occurs and add it to the list if so
        for t in solutions {
            let iPosition = particles[i].position(atTime: t)
            let jPosition = particles[j].position(atTime: t)
            if iPosition.y == jPosition.y || iPosition.z == jPosition.z {
                collisions.append((t: t, i: i, j: j))
                continue
            }
        }
    }
}

var particleActive = Array(repeating: true, count: particles.count)
var currentTime = -1
var particlesToDeactivate: Set<Int> = Set()
for collision in collisions.sorted(by: { $0.t < $1.t }) {
    if collision.t != currentTime {
        print(currentTime, particlesToDeactivate)
        for p in particlesToDeactivate {
            particleActive[p] = false
        }
        particlesToDeactivate = Set()
        currentTime = collision.t
    }

    if particleActive[collision.i] && particleActive[collision.j] {
        particlesToDeactivate.insert(collision.i)
        particlesToDeactivate.insert(collision.j)
    }
}
print(currentTime, particlesToDeactivate)
for p in particlesToDeactivate {
    particleActive[p] = false
}
print("remaining:", particleActive.reduce(0) { $0 + ($1 ? 1 : 0) })




