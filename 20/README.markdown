Day 20: Particle Swarm
======================

http://adventofcode.com/2017/day/20

I solved part 1 directly without actually running the simulation, which sucked because then part 2 **really** expected you to run the simulation. But I did that too without that, MORAL VICTORY

Part 1: the solution will be one of the particles with the lowest total acceleration. I added a tiebreaker for the lowest starting velocity based on my input having two with the same acceleration, but I'm not sure that's actually correct in all cases.

Part 2: we can calculate the position of a particle at any time `t` with a quadratic equation, and then whether two particles collide at any time by solving the quadratic formula with that info. Then we can walk through the list of possible collisions in chronological order, removing all those particles from consideration if they're still active and can collide in the first place. This might be faster in the long run than checking every pair of collisions every time step. But the final collisions for me happened at `t = 39`, which is so small that I should've just simulated it lmao
