//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "12", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

struct Node {
    let id: Int
    let edges: [Int]
}

var graph: [Int: Node] = [:]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ").compactMap { Int($0) }
    graph[components[0]] = Node(id: components[0], edges: Array(components.dropFirst()))
}


var visited: [Int: Bool] = [:]
var stack = [0]
while !stack.isEmpty {
    let id = stack.popLast()!
    if visited[id] != nil {
        continue
    }

    visited[id] = true
    for edge in graph[id]!.edges {
        stack.append(edge)
    }
}
print(visited.count)


// Part 2

print("--------------")

visited = [:]
var groups = 0
for (id, _) in graph {
    if visited[id] != nil {
        continue
    }
    groups += 1

    stack = [id]
    var currentGroupSize = 0
    while !stack.isEmpty {
        let id = stack.popLast()!
        if visited[id] != nil {
            continue
        }
        currentGroupSize += 1

        visited[id] = true
        for edge in graph[id]!.edges {
            stack.append(edge)
        }
    }
    print("group", id, currentGroupSize)
}
print("groups:", groups)
