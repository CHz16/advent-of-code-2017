Day 12: Digital Plumber
=======================

http://adventofcode.com/2017/day/12

Just some simple graph traversal questions. Part 1 just starts a depth-first search from node 0 (BFS vs. DFS doesn't really matter here) and counts how many nodes we find. Part 2 goes through every node, doing a depth-first search from each one we haven't visited yet, and counts how many searches we did to count the number of groups.

I definitely could've combined parts 1 & 2 together, but it was faster to just reimplement it with a paste of the main loop.
