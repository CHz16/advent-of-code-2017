//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "8", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var operations: [String: (Int, Int) -> Int] = ["inc": (+), "dec": (-)]
var comparisons: [String: (Int, Int) -> Bool] = ["==": (==), "!=": (!=), ">": (>), ">=": (>=), "<" : (<), "<=": (<=)]

var registers: [String: Int] = [:]
var highestEverMax = 0
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    let source = components[0]
    let operation = operations[components[1]]!
    let value = Int(components[2])!
    let check = components[4]
    let comparison = comparisons[components[5]]!
    let comparisonValue = Int(components[6])!

    if registers[source] == nil {
        registers[source] = 0
    }
    if registers[check] == nil {
        registers[check] = 0
    }

    if comparison(registers[check]!, comparisonValue) {
        registers[source] = operation(registers[source]!, value)
    }

    highestEverMax = max(highestEverMax, registers.values.max()!)
}
print(registers)
print(registers.values.max()!)


// Part 2

print("--------------")
print(highestEverMax)
