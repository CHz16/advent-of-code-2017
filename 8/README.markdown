Day 8: I Heard You Like Registers
=================================

http://adventofcode.com/2017/day/8

Just parse a whole bunch of mathematical operations and execute them all in order, nothing fancy at all.

Instead of doing a bunch of string comparisons on the arithmetic and comparison operations, I just crammed the operators into dictionaries and index into those lmao

* Part 1: 212th place (9:51)
* Part 2: 208th place (11:05)
