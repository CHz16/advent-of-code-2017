Day 4: High-Entropy Passphrases
===============================

http://adventofcode.com/2017/day/4

For both parts of this we just detect unique words by jamming every word into a set and letting that cull duplicates, and then comparing the total number of words with how many are in the set. If they don't match, then there was at least one duplicate word, so we can reject that.

In the second part, we detect anagrams by sorting all the letters in the words before adding them to the set. Anagrams will end up with the same string after this process.

* Part 1: 315th place (3:42)
* Part 2: 327th place (7:34)
