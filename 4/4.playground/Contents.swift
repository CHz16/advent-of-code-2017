//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "4", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var valid = 0
input.enumerateLines { (line, stop) in
    let words = line.components(separatedBy: " ")
    let uniqueWords = Set(words)
    if words.count == uniqueWords.count {
        valid += 1
        print(line)
    }
}
print(valid)


// Part 2

print("--------------")

valid = 0
input.enumerateLines { (line, stop) in
    let words = line.components(separatedBy: " ")
    let uniqueWords = Set<String>(words.map { String($0.sorted()) })
    if words.count == uniqueWords.count {
        valid += 1
        print(line)
    }
}
print(valid)
