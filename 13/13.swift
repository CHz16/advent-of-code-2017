#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "13.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

var layers: [Int: Int] = [:]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ").compactMap { Int($0) }
    layers[components[0]] = components[1]
}

var totalSeverity = 0
for (layer, height) in layers {
    let period = (height - 1) * 2
    if layer % period == 0 {
        totalSeverity += layer * height
    }
}
print(totalSeverity)


// Part 2

print("--------------")

var offset = 0
outer: while true {
    for (layer, height) in layers {
        let period = (height - 1) * 2
        if (layer + offset) % period == 0 {
            print("delay", offset, "caught in", layer)
            offset += 1
            continue outer
        }
    }
    break outer
}
print(offset, "got through")
