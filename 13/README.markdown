Day 13: Packet Scanners
=======================

http://adventofcode.com/2017/day/13

It's pretty easy to check if you're caught in a specific layer with modulo arithmetic: `(layer + offset) % period == 0`.

I fumbled the second part because I didn't stop to think the problem through: it asks for the lowest delay where you don't get caught, not the lowest delay where the severity is 0. If you get caught only in layer 0, then your total severity is 0, but that's not a valid answer.

* Part 1: 56th place (8:53)
* Part 2: 93rd place (21:21)
