#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "22.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

enum NodeState { case weakened, infected, flagged, clean }

var grid: [Int: [Int: NodeState]] = [:]
var row = 0
input.enumerateLines { (line, stop) in
    var elements: [Int: NodeState] = [:]
    for (col, char) in line.enumerated() {
        elements[col] = (char == "." ? .clean : .infected)
    }
    grid[row] = elements
    row += 1
}
let gridCopy = grid

row = grid.count / 2
var col = grid[0]!.count / 2
var rowDelta = -1, colDelta = 0
var infectionBursts = 0
for _ in 0..<10000 {
    if grid[row] == nil {
        grid[row] = [:]
    }
    if grid[row]?[col] == nil {
        grid[row]![col] = .clean
    }

    if grid[row]![col]! == .infected {
        grid[row]![col] = .clean
        if rowDelta == 0 {
            (rowDelta, colDelta) = (colDelta, rowDelta)
        } else {
            (rowDelta, colDelta) = (-colDelta, -rowDelta)
        }
    } else {
        infectionBursts += 1
        grid[row]![col] = .infected
        if rowDelta == 0 {
            (rowDelta, colDelta) = (-colDelta, -rowDelta)
        } else {
            (rowDelta, colDelta) = (colDelta, rowDelta)
        }
    }

    row += rowDelta
    col += colDelta
}
print(infectionBursts)


// Part 2

print("--------------")

grid = gridCopy
row = grid.count / 2
col = grid[0]!.count / 2
rowDelta = -1
colDelta = 0
infectionBursts = 0
for i in 0..<10000000 {
    if i % 500000 == 0 {
        print(infectionBursts)
    }

    if grid[row] == nil {
        grid[row] = [:]
    }
    if grid[row]?[col] == nil {
        grid[row]![col] = .clean
    }

    switch grid[row]![col]! {
    case .infected:
        grid[row]![col] = .flagged
        if rowDelta == 0 {
            (rowDelta, colDelta) = (colDelta, rowDelta)
        } else {
            (rowDelta, colDelta) = (-colDelta, -rowDelta)
        }
    case .clean:
        grid[row]![col] = .weakened
        if rowDelta == 0 {
            (rowDelta, colDelta) = (-colDelta, -rowDelta)
        } else {
            (rowDelta, colDelta) = (colDelta, rowDelta)
        }
    case .weakened:
        infectionBursts += 1
        grid[row]![col] = .infected
    case .flagged:
        grid[row]![col] = .clean
        (rowDelta, colDelta) = (-rowDelta, -colDelta)
    }

    row += rowDelta
    col += colDelta
}
print(infectionBursts)
