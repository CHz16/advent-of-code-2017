Day 22: Sporifica Virus
=======================

http://adventofcode.com/2017/day/22

I wonder how slow my "trick" of growing the infinite plane by just using hash tables instead of arrays is.

* Part 1: 209th place (20:21)
* Part 2: 262nd place (33:19)
