//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "9", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

input.enumerateLines { (line, stop) in
    var level = 0, score = 0
    var noncanceledGarbage = 0

    var garbage = false, cancel = false
    for character in line {
        if cancel {
            cancel = false
            continue
        }

        if character == "!" {
            cancel = true
        } else if garbage {
            if character == ">" {
                garbage = false
            } else {
                noncanceledGarbage += 1
            }
        } else {
            if character == "<" {
                garbage = true
            } else if character == "{" {
                level += 1
            } else if character == "}" {
                score += level
                level -= 1
            }
        }
    }

    print(score, noncanceledGarbage, line)
}

