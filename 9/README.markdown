Day 9: Stream Processing
========================

http://adventofcode.com/2017/day/9

Pretty simple parsing exercise here:

* If we encounter an `!`, then skip the next character.
* If we're in the normal state and we encounter a `<`, then move to the garbage state.
* If we're in the garbage state and we encounter a `>`, then move back to the normal state. Otherwise, add 1 to the non-canceled garbage character count (part 2).
* If we're in the normal state and we encounter a `{`, then add 1 to the group level.
* If we're in the normal state and we encounter a `}`, then add the current group level to the total score and subtract 1 from the group level.

This completely ignores the commas used to separate groups, which it turns out are irrelevant to the calculations and parsing.

I started like 15-20 minutes late on this one so I didn't record my times, but I was close enough to the start that I still placed anyway. This took maybe about 10 minutes for part 1 and another 2 minutes for part 2.

* Part 1: 784th place
* Part 2: 720th place
