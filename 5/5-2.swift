#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "5.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 2

var maze = input.components(separatedBy: "\n").compactMap { Int($0) }
var index = 0
var steps = 0
while index > -1 && index < maze.count {
    let offset = maze[index]
    maze[index] += maze[index] >= 3 ? -1 : 1
    index += offset
    steps += 1
}
print(maze)
print(steps)
