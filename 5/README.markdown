Day 5: A Maze of Twisty Trampolines, All Alike
==============================================

http://adventofcode.com/2017/day/5

Just a completely straightforward implementation of the puzzle spec, nothing to comment on here. Besides that the number of jumps executed in both parts is pretty high, so I had to drop down to a command-line script for the first time on this one.

* Part 1: 391st place (6:08)
* Part 2: 348th place (7:50)
