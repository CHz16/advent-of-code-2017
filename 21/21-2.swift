#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "21.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct Pattern: Hashable, CustomStringConvertible {
    let string: String
    let size: Int

    init(_ string: String) {
        self.string = string
        if string.count == 4 {
            self.size = 2
        } else if string.count == 9 {
            self.size = 3
        } else {
            self.size = 4
        }
    }

    var lightsOn: Int {
        let count = string.reduce(0) { $0 + ($1 == "#" ? 1 : 0) }
        return count
    }

    var rotated: Pattern {
        var newString = ""
        for col in 0..<size {
            for row in (0..<size).reversed() {
                newString.append(string[string.index(string.startIndex, offsetBy: row * size + col)])
            }
        }
        return Pattern(newString)
    }
    var flipped: Pattern {
        var newString = ""
        var index = string.startIndex
        for _ in 0..<size {
            let nextIndex = string.index(index, offsetBy: size)
            newString += String(string[index..<nextIndex].reversed())
            index = nextIndex
        }

        return Pattern(newString)
    }

    func transformations() -> Set<Pattern> {
        let ninety = self.rotated
        let oneEighty = ninety.rotated
        let twoSeventy = oneEighty.rotated
        return Set<Pattern>([self, self.flipped, ninety, ninety.flipped, oneEighty, oneEighty.flipped, twoSeventy, twoSeventy.flipped])
    }


    var rows: [String] {
        var rows: [String] = []
        var index = string.startIndex
        for _ in 0..<size {
            let nextIndex = string.index(index, offsetBy: size)
            rows.append(String(string[index..<nextIndex]))
            index = nextIndex
        }
        return rows
    }
    var description: String {
        return self.rows.joined(separator: "\n")
    }
}

struct Image: CustomStringConvertible {
    let grid: [[Pattern]]

    var lightsOn: Int {
        let count = grid.reduce(0) { $0 + $1.map { $0.lightsOn }.reduce(0, +) }
        return count
    }

    var enhanced: Image {
        var newGrid: [[Pattern]]
        if grid[0][0].size == 3 && grid.count % 2 == 1 {
            newGrid = grid
        } else if grid[0][0].size == 2  {
            newGrid = grid
        } else {
            let stringRows = self.rows
            let newPatternSize = stringRows.count % 2 == 0 ? 2 : 3
            newGrid = []
            for r in stride(from: 0, to: stringRows.count, by: newPatternSize) {
                var indices = (0..<newPatternSize).map { stringRows[r + $0].startIndex }
                var newRow: [Pattern] = []
                for _ in 0..<(stringRows.count / newPatternSize) {
                    let nextIndices = indices.enumerated().map { stringRows[r + $0].index($1, offsetBy: newPatternSize) }
                    var slice = ""
                    for i in 0..<newPatternSize {
                        slice += String(stringRows[r + i][indices[i]..<nextIndices[i]])
                    }
                    newRow.append(Pattern(slice))
                    indices = nextIndices
                }
                newGrid.append(newRow)
            }
        }
        return Image(grid: newGrid.map { $0.map { rules[$0]! } })
    }

    var rows: [String] {
        var stringRows: [String] = []
        for patternRow in grid {
            let decomposedPatterns = patternRow.map { $0.rows }
            for i in 0..<decomposedPatterns[0].count {
                stringRows.append(decomposedPatterns.map { $0[i] }.joined())
            }
        }
        return stringRows
    }
    var description: String {
        return self.rows.joined(separator: "\n")
    }
}


// Parts 1 & 2

var rules: [Pattern: Pattern] = [:]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")

    let inputs = Pattern(components[0]).transformations()
    let output = Pattern(components[1])
    for input in inputs {
        rules[input] = output
    }
}

var products: [Pattern: [(pattern: Pattern, count: Int)]] = [:]
var lightCounts: [[Pattern: Int]] = [[:], [:], [:]]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    if components[0].count != 9 {
        return
    }

    let basePattern = Pattern(components[0])
    var images = [Image(grid: [[basePattern]]).enhanced]
    images.append(images[0].enhanced)
    images.append(images[1].enhanced)
    var nextPatternCounts: [Pattern: Int] = [:]
    for row in images[2].grid {
        for pattern in row {
            if nextPatternCounts[pattern] == nil {
                nextPatternCounts[pattern] = 1
            } else {
                nextPatternCounts[pattern]! += 1
            }
        }
    }
    let product = nextPatternCounts.map { (pattern: $0, count: $1) }

    let lightsOn = [basePattern.lightsOn, images[0].lightsOn, images[1].lightsOn]
    for input in basePattern.transformations() {
        lightCounts[0][input] = lightsOn[0]
        lightCounts[1][input] = lightsOn[1]
        lightCounts[2][input] = lightsOn[2]
        products[input] = product
    }
}

var image = [Pattern(".#...####"): 1]
for i in 0..<20 {
    print(i * 3, image.reduce(0) { $0 + lightCounts[0][$1.0]! * $1.1 })
    if i == 19 {
        break
    }
    print(i * 3 + 1, image.reduce(0) { $0 + lightCounts[1][$1.0]! * $1.1 })
    print(i * 3 + 2, image.reduce(0) { $0 + lightCounts[2][$1.0]! * $1.1 })

    image = image.reduce([:]) { (newImage, sourcePatternAndCount) in
        var newImage = newImage
        for (pattern, count) in products[sourcePatternAndCount.0]! {
            if newImage[pattern] == nil {
                newImage[pattern] = count * sourcePatternAndCount.1
            } else {
                newImage[pattern]! += count * sourcePatternAndCount.1
            }
        }
        return newImage
    }
}

