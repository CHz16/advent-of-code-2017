Day 21: Fractal Art
===================

http://adventofcode.com/2017/day/21

I'm sure there is totally a way to look at the underlying structure of the enhancement procedure and do extremely efficient math, but my code completes parts 1 & 2 in 16 seconds with brute force so lmao sure why not

Also please try to ignore the horrible `map`/`reduce` abuse here and there

Addendum
--------

I had an idea for speedy generation with the following two observations:

* You start with an image of size `3 * 3^n` (where `n = 0`), and from there successively get images of `4 * 3^n`, `6 * 3^n`, and `9 * 3^n = 3 * 3^m`, where `m = n + 1`).
* During the first step going from `3` to `4`, you split the grid up into 3x3 patterns, and each of those develop independently of one another.

The second part is critical, because it means that we never actually need to store the full image at any point in time. All we need to know is how many of each 3x3 pattern appears in the image, and from that we can determine how many of each 3x3 pattern appears in the image three steps ahead. We can do that by precalculating the 9x9 image that each 3x3 pattern will develop into after being enhanced three times and then storing the breakdown of the 9 patterns in that image for later. We can also precalculate how many lights are on in each 3x3 pattern, so each step is just doing a little bit of simple arithmetic and it's super speedy.

The problem is that this process only "makes" every third image, but since each 3x3 pattern develops independently, we can also precalculate how many lights will be on for each pattern if it's enhanced only once or twice, and then we can use that to calculate all the off values too.

That's implemented in `21-2.swift`, and it generates all counts from 0-57 in a bit under two seconds! It stops there because we overflow `Int` at 58 lmao
