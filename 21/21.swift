#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "21.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct Pattern: Hashable, CustomStringConvertible {
    let string: String
    let size: Int

    init(_ string: String) {
        self.string = string
        if string.count == 4 {
            self.size = 2
        } else if string.count == 9 {
            self.size = 3
        } else {
            self.size = 4
        }
    }

    var lightsOn: Int {
        let count = string.reduce(0) { $0 + ($1 == "#" ? 1 : 0) }
        return count
    }

    var rotated: Pattern {
        var newString = ""
        for col in 0..<size {
            for row in (0..<size).reversed() {
                newString.append(string[string.index(string.startIndex, offsetBy: row * size + col)])
            }
        }
        return Pattern(newString)
    }
    var flipped: Pattern {
        var newString = ""
        var index = string.startIndex
        for _ in 0..<size {
            let nextIndex = string.index(index, offsetBy: size)
            newString += String(string[index..<nextIndex].reversed())
            index = nextIndex
        }

        return Pattern(newString)
    }

    func transformations() -> Set<Pattern> {
        let ninety = self.rotated
        let oneEighty = ninety.rotated
        let twoSeventy = oneEighty.rotated
        return Set<Pattern>([self, self.flipped, ninety, ninety.flipped, oneEighty, oneEighty.flipped, twoSeventy, twoSeventy.flipped])
    }


    var rows: [String] {
        var rows: [String] = []
        var index = string.startIndex
        for _ in 0..<size {
            let nextIndex = string.index(index, offsetBy: size)
            rows.append(String(string[index..<nextIndex]))
            index = nextIndex
        }
        return rows
    }
    var description: String {
        return self.rows.joined(separator: "\n")
    }
}

struct Image: CustomStringConvertible {
    let grid: [[Pattern]]

    var lightsOn: Int {
        let count = grid.reduce(0) { $0 + $1.map { $0.lightsOn }.reduce(0, +) }
        return count
    }

    var enhanced: Image {
        var newGrid: [[Pattern]]
        if grid[0][0].size == 3 && grid.count % 2 == 1 {
            newGrid = grid
        } else if grid[0][0].size == 2  {
            newGrid = grid
        } else {
            let stringRows = self.rows
            let newPatternSize = stringRows.count % 2 == 0 ? 2 : 3
            newGrid = []
            for r in stride(from: 0, to: stringRows.count, by: newPatternSize) {
                var indices = (0..<newPatternSize).map { stringRows[r + $0].startIndex }
                var newRow: [Pattern] = []
                for _ in 0..<(stringRows.count / newPatternSize) {
                    let nextIndices = indices.enumerated().map { stringRows[r + $0].index($1, offsetBy: newPatternSize) }
                    var slice = ""
                    for i in 0..<newPatternSize {
                        slice += String(stringRows[r + i][indices[i]..<nextIndices[i]])
                    }
                    newRow.append(Pattern(slice))
                    indices = nextIndices
                }
                newGrid.append(newRow)
            }
        }
        return Image(grid: newGrid.map { $0.map { rules[$0]! } })
    }

    var rows: [String] {
        var stringRows: [String] = []
        for patternRow in grid {
            let decomposedPatterns = patternRow.map { $0.rows }
            for i in 0..<decomposedPatterns[0].count {
                stringRows.append(decomposedPatterns.map { $0[i] }.joined())
            }
        }
        return stringRows
    }
    var description: String {
        return self.rows.joined(separator: "\n")
    }
}


// Part 1

var rules: [Pattern: Pattern] = [:]
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")

    let inputs = Pattern(components[0]).transformations()
    let output = Pattern(components[1])
    for input in inputs {
        rules[input] = output
    }
}

var image = Image(grid: [[Pattern(".#...####")]])
for _ in 0..<5 {
    print(image.lightsOn)
    print(image, "\n")
    image = image.enhanced
}
print(image.lightsOn)
print(image)


// Part 2

print("--------------")

for _ in 0..<13 {
    image = image.enhanced
    print(image.lightsOn)
}

