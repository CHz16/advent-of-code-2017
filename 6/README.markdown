Day 6: Memory Reallocation
==========================

http://adventofcode.com/2017/day/6

I got the wrong answer on this one for like 15 minutes because I failed to copy the last character of the puzzle input somehow, which was gravely unfortunate.

And then the second part I calculated by hand by copying the output to a text file and doing a search, but I was off by one and had a minute penalty there too. Not my night!

* Part 1: 1111th place (29:57)
* Part 2: 970th place (32:14)
