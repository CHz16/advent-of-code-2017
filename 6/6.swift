#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "6.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

var banks = input.components(separatedBy: .whitespacesAndNewlines).compactMap { Int($0) }
print(banks)

var cycles = 0
var visitedStates = Set([banks.description])
while true {
    var largestBank = 0
    for (i, blocks) in banks.enumerated() {
        if blocks > banks[largestBank] {
            largestBank = i
        }
    }

    var blocks = banks[largestBank]
    banks[largestBank] = 0
    var i = largestBank
    while blocks > 0 {
        i = (i + 1) % banks.count
        banks[i] += 1
        blocks -= 1
    }

    cycles += 1
    print(banks)

    let hash = banks.description
    if visitedStates.contains(hash) {
        break
    }
    visitedStates.insert(hash)
}
print(cycles)


// Part 2

print("--------------")
